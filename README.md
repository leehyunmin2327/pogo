# Welcome to Pogo

### Pogo is a GUI application for generating Tango projects.
 See Wiki for more insformation

#### Pogo-9.7 to 9.8 need to be compiled and executed with JAVA 11 to JAVA 15.
[Download](https://repo1.maven.org/maven2/org/tango-controls/Pogo/9.8.0/Pogo-9.8.0.jar) Latest release

#### The latest release compatible with java 8 is  Pogo-9.6.31
[Download](https://gitlab.com/tango-controls/pogo/-/package_files/56294796/download)
Latest release **java 8** compatible

#### See Pogo documentation
[![Docs](https://img.shields.io/badge/Latest-Docs-orange.svg)](https://tango-controls.readthedocs.io/en/latest/tools-and-extensions/built-in/pogo/index.html)


